import System.IO (BufferMode(..), hSetBuffering, stdout, stderr)
import qualified Control.Monad as Control.Monad
import qualified System.Exit as System.Exit

import qualified Test.PreBasic as Test.PreBasic
import qualified Test.Basic as Test.Basic
import qualified Test.HuttonsRazor as Test.HuttonsRazor
import qualified Test.DiffRecord as Test.DiffRecord
import qualified Test.TextTakeEnd as Test.TextTakeEnd
import qualified Test.PropertyTesting as Test.PropertyTesting

-- import qualified Test.Example.Exception as Test.Example.Exception
-- import qualified Test.Example.QuickCheck as Test.Example.QuickCheck
-- import qualified Test.Example.References as Test.Example.References
-- import qualified Test.Example.Registry as Test.Example.Registry
-- import qualified Test.Example.Resource as Test.Example.Resource
-- import qualified Test.Example.Roundtrip as Test.Example.Roundtrip
-- import qualified Test.Example.STLC as Test.Example.STLC
-- import qualified Test.PropertyTesting as Test.PropertyTesting

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  hSetBuffering stderr LineBuffering

  results <- sequence [
    Test.PreBasic.tests
    , Test.Basic.tests
    , Test.HuttonsRazor.tests
    , Test.DiffRecord.tests
    , Test.TextTakeEnd.tests
    , Test.PropertyTesting.tests
    
    -- , Test.Example.Exception.tests
    -- , Test.Example.QuickCheck.tests
    -- , Test.Example.References.tests
    -- , Test.Example.Registry.tests
    -- , Test.Example.Resource.tests
    -- , Test.Example.Roundtrip.tests
    -- , Test.Example.STLC.tests
    -- , Test.PropertyTesting.tests
    ]

  Control.Monad.unless (and results) $
    System.Exit.exitFailure
  pure ()
