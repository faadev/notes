{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}

module Test.DiffRecord where

import Hedgehog (Gen, Property, property, forAll 
                , checkParallel, discover, (===), (/==))
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import DiffRecord (SomeRecord(..))

genRecord :: Gen SomeRecord
genRecord =
  SomeRecord
    <$> Gen.int (Range.linearFrom 0 (-1000) 1000)
    <*> Gen.bool
    <*> Gen.double (Range.linearFrac 7.2 15.9)
    <*> Gen.list (Range.linear 5 100)
          ((,)
            <$> Gen.int (Range.constant 0 10)
            <*> Gen.string (Range.constant 2 4) Gen.alpha)

prop_record :: Property
prop_record =
  property $ do
    x <- forAll genRecord
    y <- forAll genRecord
    x === y

prop_different_record :: Property
prop_different_record =
  property $ do
    x <- forAll genRecord
    x /== x

tests :: IO Bool
tests =
  checkParallel $$(discover)
