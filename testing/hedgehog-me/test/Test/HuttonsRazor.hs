{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}

module Test.HuttonsRazor where

import Hedgehog (Gen, Property, property, forAll, assert, success
                , checkParallel, discover)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import HuttonsRazor (Exp(..), evalExp)

-- | The subterm combinators (Gen.subterm, Gen.subtermM, Gen.subterm2,
-- Gen.subterm2M, etc) allow a generator to shrink to one of its
-- sub-terms.
genExp1 :: Gen Exp
genExp1 =
  Gen.recursive Gen.choice [Lit <$> Gen.int (Range.linear 0 10000)]
  [Gen.subterm2 genExp1 genExp1 Add]

prop_hutton_1 :: Property
prop_hutton_1 =
  property $ do
    x <- forAll genExp1
    case x of
      Add (Add _ _) _ -> assert (evalExp x < 100)
      _ -> success

-- Gen.shrink is a more general way to add shrinks to a generator.

shrinkExp2 :: Exp -> [Exp]
shrinkExp2 = \case
  Lit _ -> []
  Add x y -> [Lit (evalExp (Add x y))]

genExp2 :: Gen Exp
genExp2 =
  Gen.shrink shrinkExp2 $
  Gen.recursive Gen.choice [Lit <$> Gen.int (Range.linear 0 10000)]
  [Gen.subterm2 genExp2 genExp2 Add]

prop_hutton_2 :: Property
prop_hutton_2 =
  property $ do
    x <- forAll genExp2
    case x of
      Add (Add _ _) _ -> assert (evalExp x < 100)
      _ -> success  
  

tests :: IO Bool
tests =
  checkParallel $$(discover)
