{-# LANGUAGE TemplateHaskell #-}

module Test.PreBasic where

import Hedgehog (Property
                , property, success, discard, failure
                , withTests, withDiscards, withShrinks
                , forAll, assert
                , checkParallel, discover)

import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Control.Monad (guard)

-- property :: PropertyT IO () -> Property
-- success :: MonadTest m => m ()
-- discard :: Monad m => PropertyT m a
-- failure :: MonadTest m => m a

prop_success :: Property
prop_success = property success

-- by default discards 100 testst
prop_discard :: Property
prop_discard = property discard

prop_failure :: Property
prop_failure = property failure

prop_test_limit :: Property
prop_test_limit = withTests 1000 . property $ success

prop_discard_limit :: Property
prop_discard_limit =
  withDiscards 1000 . property $ discard

prop_shrink_limit :: Property
prop_shrink_limit =
  withShrinks 0 . property $ do
    x <- forAll $ Gen.enum 'a' 'z'
    assert $ x == 'z'

prop_foo :: Property
prop_foo =
  property $ do
    x <- forAll $ Gen.enum 'a' 'z'
    y <- forAll $
      Gen.choice [
          Gen.integral (Range.linear 0 1000)
        , Gen.integral (Range.linear 0 1000)
        ]

    guard (y `mod` 2 == (1 :: Int))

    assert $
      y < 87 && x <= 'r'

tests :: IO Bool
tests =
  checkParallel $$(discover)

{-
{-# LANGUAGE OverloadedStrings #-}

tests :: IO Bool
tests =
  checkParallel $ Group "Test.Example" [
      ("prop_reverse", prop_reverse)
    ]
-}
