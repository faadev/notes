{-# LANGUAGE TemplateHaskell #-}

module Test.PropertyTesting where

import qualified Data.List as List
import Hedgehog (Gen, Property, discover, property, forAll, (===), checkParallel)
import Hedgehog.Gen as Gen
import Hedgehog.Range as Range
import FauxReverse (fauxReverse)

-- Property testing

-- | Generate a list of Int with the following characteristics:
-- lenght from 0 to 10000 
-- elements value is bounded by minBound and maxBound of Int
genIntList :: Gen [Int]
genIntList =
  let
    -- Range.linear :: Integral a => a -> a -> Range a
    -- Range.linear :: Range Int
    listLength = Range.linear 0 10000
  in
    -- Gen.list :: MonadGen m => Range Int -> m a -> m [a]
    -- Gen.list :: Range Int -> Gen Int -> Gen [Int]
    -- Gen.enumBounded :: (MonadGen m, Enum a, Bounded a) => m a
    -- Gen.enumBounded :: Gen Int
    Gen.list listLength Gen.enumBounded

prop_reverse :: Property
prop_reverse =
  property $ do
    xs <- forAll genIntList
    List.reverse (List.reverse xs) === xs

-- Shrinking constructively

-- Generators made out of Hedgehog.Range are constructive; Range
-- contains enough information to generate a correct value every time,
-- and each Range implies a shrink step and gradient.

-- To demonstrate how genIntList shrinks: 
prop_fauxReverse :: Property
prop_fauxReverse =
  property $ do
    xs <- forAll genIntList
    fauxReverse xs === List.reverse xs

-- We might like to write a more precise property to protect against
-- dropped elements. A reverse function should preserve every element
-- in the list.
prop_fauxReverse_length :: Property
prop_fauxReverse_length =
  property $ do
    xs <- forAll genIntList
    length (fauxReverse xs) === length xs


-- Predicates and failure

-- We may wish to write generators in terms of negative invariants. In
-- practice, many invariants can be expressed constructively in terms
-- of ranges and bind. Regardless, negative generators that discard
-- are necessary when a constructive generator is not feasible.

-- A generator for sized lists of unique elements. Elements must be
-- valued between 0 and 10, and the list's length must be between 0
-- and 10:
genUniqueIntList :: Gen [Int]
genUniqueIntList = do
  let listLength = Range.linear 0 10
      genInt = Gen.int (Range.linear 0 10)
  xs <- Gen.list listLength genInt
  if List.nub xs == xs
    then pure xs
    else Gen.discard -- empty / mzero

tests :: IO Bool
tests =
  checkParallel $$(discover)

{-
{-# LANGUAGE OverloadedStrings #-}

tests :: IO Bool
tests =
  checkParallel $ Group "Test.Example" [
      ("prop_reverse", prop_reverse)
    ]
-}
