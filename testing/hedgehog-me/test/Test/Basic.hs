{-# LANGUAGE TemplateHaskell #-}

module Test.Basic where

import Hedgehog (Property, Gen
                , property, forAll, (===), checkParallel, discover)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Basic (Order(..), Item(..), Product(..), USD(..)
             , total, merge)

-- | An Item generator with Product "sandwich" or "noodles" and USD
-- from 5 to 10
cheap :: Gen Item
cheap = Item
    <$> (Product <$> Gen.element ["sandwich", "noodles"])
    <*> (USD <$> Gen.integral (Range.constant 5 10))

-- | An Item generator with Product "oculus" or "vivel" and USD from
-- 1000 to 2000
expensive :: Gen Item
expensive =
  Item
    <$> (Product <$> Gen.element ["oculus", "vive"])
    <*> (USD <$> Gen.integral (Range.linear 1000 2000))

-- | An Order generator given an Item generator: chooses from a list
-- of Items of length from 0 to 50
order :: Gen Item -> Gen Order
order gen = Order <$> Gen.list (Range.linear 0 50) gen

prop_total :: Property
prop_total =
  property $ do
    x <- forAll (order $ Gen.choice [cheap, expensive])
    y <- forAll (order expensive)
    total (merge x y) === total x + total y

tests :: IO Bool
tests =
  checkParallel $$(discover)

