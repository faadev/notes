{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}

module Test.TextTakeEnd where

import Hedgehog (Property, property, forAll 
                , checkParallel, discover, (===))
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import qualified Data.List as List
import qualified Data.Text as Text

prop_takeEnd :: Property
prop_takeEnd =
  property $ do
    xs <- forAll $ Gen.string (Range.linear 0 100) Gen.unicode
    n <- forAll $ Gen.int (Range.linear 0 100)
    let
      string = List.reverse . List.take (fromIntegral n) $ List.reverse xs
      text = Text.unpack . Text.takeEnd n $ Text.pack xs
    string === text

tests :: IO Bool
tests =
  checkParallel $$(discover)
