{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Basic where

-- | Product is a String
newtype Product = Product String
  deriving (Eq, Ord, Show)

-- | USD is an Int
newtype USD = USD Int
  deriving (Eq, Ord, Show, Num, Enum, Real, Integral)

-- | Item is a Product and a USD
data Item = Item Product USD
  deriving (Eq, Ord, Show)

-- | Order is a list of Item
newtype Order = Order [Item]
  deriving (Eq, Ord, Show)

-- | The price of an item
price :: Item -> USD
price (Item _ x) = x

-- | Merge two Order in a single order: the list of Item is the
-- concatenation of the two list and, if there is a price > 50 and a
-- price < 50 in the first list and a price > 1050 in the second list,
-- a processing item.
merge :: Order -> Order -> Order
merge (Order xs) (Order ys) =
  Order $ xs ++ ys ++
    if any ((< 50) . price) xs &&
       any ((> 50) . price) xs &&
       any ((> 1050) . price) ys then
      [Item (Product "processing") (USD 1)]
    else
      []

-- | The sum of the prices for a given Order
total :: Order -> USD
total (Order xs) = sum $ fmap price xs


      
