{-# LANGUAGE LambdaCase #-}

module HuttonsRazor where

data Exp =
  Lit !Int
  | Add !Exp !Exp
  deriving (Eq, Ord, Show)

evalExp :: Exp -> Int
evalExp = \case
  Lit x -> x
  Add x y -> evalExp x + evalExp y
