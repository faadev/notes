module DiffRecord where

data SomeRecord = SomeRecord {
  someInt :: Int
  , someBool :: Bool
  , someDouble :: Double
  , someList :: [(Int, String)]
  } deriving (Eq, Show)
