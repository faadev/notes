module BuildingBlocks.LazyEvaluation where

import System.Random (StdGen, random, getStdGen)

-- Thanks to lazy evaluation, we can still consume the undoomed part
-- of this list:

doomedList :: [Integer]
doomedList = [2, 3, 5, 7, undefined]

-- The take function is lazy because the cons operator (:) is lazy
-- (because all functions in Haskell are lazy by default). A lazy cons
-- evaluates only its first argument, while the second argument, the
-- tail, is only evaluated when it is selected. (For strict lists,
-- both head and tail are evaluated at the point of construction of
-- the list.)

take' :: (Eq t, Num t) => t -> [a] -> [a]
take' 0 _ = []
take' n (x:xs) = x : (take' (n - 1) xs)
take' _ [] = []

-- The proxy pattern has several motivations, one of which is to defer
-- evaluation; this aspect of the proxy pattern is subsumed by lazy
-- evaluation.

c1 :: IO ()
c1 = do
  print (take 4 doomedList)

{-
 Streams
-}

-- laziness enables self-reference
infinite42s :: [Integer]
infinite42s = 42 : infinite42s  

potentialBoom :: [Integer]
potentialBoom = take 5 infinite42s

-- A stream is always just one element cons'ed to a tail of whatever
-- size. A function such as take consumes its input stream but is
-- decoupled from the producer of the stream to such an extent that it
-- doesn't matter whether the stream is finite or infinite
-- (unbounded).

generate :: StdGen -> (Int, StdGen)
generate g = random g :: (Int, StdGen)

st1 :: IO ()
st1 = do
  gen0 <- getStdGen
  let (int0, gen1) = (generate gen0)
  print int0
  let (int1, gen2) = (generate gen1)
  print int1
  let (int2, _) = (generate gen1)
  print int2
  let (int3, _) = (generate gen2)
  print int3

-- Carrying along the generator from one call to the next pollutes our
-- code and makes our intent less clear. Let's instead create a
-- producer of random integers as a stream
randInts' :: StdGen -> [(Int, StdGen)]
randInts' g =
  (randInt, g) : (randInts' nextGen)
  where (randInt, nextGen) = (generate g)

randInts :: StdGen -> [Int]
randInts g = map fst (randInts' g)

st2 :: IO ()
st2 = do
  g <- getStdGen
  print (take 3 (randInts g))
  print (take 3 (randAmounts g))

-- derive a stream of random numbers between 0 and 100
randAmounts :: StdGen -> [Int]
randAmounts g =
  -- map (\x -> x `mod` 100) (randInts g)  
  map (`mod` 100) (randInts g)  

-- This is why it is said that lazy lists decouple consumers from
-- producers. From another perspective, we have a decoupling between
-- iteration and termination. Either way, we have decoupling, which
-- means we have a new way to modularize and structure our code.

{-
 Modeling Change with Streams
-}

-- Consider a banking system, where we want to record the current
-- balance of a customer's account. In a non-pure functional language,
-- we would typically model this with a mutable variable for the
-- balance: for each debit and credit in the "real world", we would
-- mutate the balance variable.

-- Yes, we can describe the evolution of a variable as a function of
-- time. Instead of a mutable variable for bank balance, we have a
-- sequence of balance values. In other words, we replace a mutable
-- variable with the entire history of states.

bankAccount :: Num t => t -> [t] -> [t]
bankAccount openingB [] = [openingB]
bankAccount openingB (amt : amts) =
  openingB : bankAccount (openingB + amt) amts

st3 :: [Integer]
st3 = take 10 (bankAccount 0 [-100, 50, 50, 1])


