module BuildingBlocks.CurryingFunctions where

import BuildingBlocks.HigherOrderFunctions (square)

-- By default functions are curried

greetCurried :: [Char] -> [Char] -> [Char]
greetCurried title name =
  "Greetings " <> title <> " " <> name

greetUncurried :: ([Char], [Char]) -> [Char]
greetUncurried (title, name) =
  "Greetings " <> title <> " " <> name

-- A function with the first argument fixed

greetCurried' :: [Char] -> [Char]
greetCurried' = greetCurried "Ms"

greetUncurried' :: [Char] -> [Char]
greetUncurried' name = greetUncurried ("Ms", name)

-- Curried functions are composable, whereas uncurried functions are
-- not. Currying is a powerful tool for decoupling: functions become
-- independent by the arguments.

cf2 :: Integer
cf2 = cf3 11
  -- max :: Ord a => a -> a -> a
  -- uncurry :: (a -> b -> c) -> (a, b) -> c
  -- max is curried but g returns a tuple, so we have to uncurry max
  -- to be composable with g
  where
    cf3 = uncurry max . g
    g n = (n ^ (2 :: Integer), n ^ (3 :: Integer)) 

-- If map was uncurried (map' = uncurry map :: (a -> b, [a]) -> [b])
-- we couldn't define the following:
cf4 :: [[Integer]]
cf4 = map f1 [[1], [2, 2], [3, 3, 3]]
  where
    f1 :: [Integer] -> [Integer]
    f1 = map square
