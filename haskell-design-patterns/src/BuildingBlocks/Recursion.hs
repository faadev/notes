module BuildingBlocks.Recursion where

{-
 Recursion: can refer to syntax (a function or a type referring to
 itself) or to the execution process.
-}

-- Non-tail recursion

sumNonTail :: Num a => [a] -> a
sumNonTail [] = 0
sumNonTail (x : xs) = x + (sumNonTail xs)

{-
 sumNonTail [2, 3, 5, 7]
 2 + sumNonTail [3, 5, 7]
 2 + (3 + sumNonTail [5, 7])
 2 + (3 + (5 + sumNonTail [7]))
 2 + (3 + (5 + (7 + sumNonTail [])))
 2 + (3 + (5 + (7 + 0)))
 2 + (3 + (5 + 7))
 2 + (3 + 12)
 2 + 15
 17

 sumNonTail is non-tail recursive because it express a recursive
 process and we need to hold the entire list in memory to perform the
 sum.
-}

-- Tail recursion

sumTail :: [Integer] -> Integer
sumTail = sumTail' 0
  where
    sumTail' acc [] = acc
    sumTail' acc (x : xs) = sumTail' (acc + x) xs
{-
 sumTail [2, 3, 5, 7]
 sumTail' 0 [2, 3, 5, 7]
 sumTail' 2 [3, 5, 7]
 sumTail' 5 [5, 7]
 sumTail' 10 [7]
 sumTail' 17 []
 17

 sumTail is tail recursive because it express an iterative process and
 we can take advantage at the use of 'constant space'.
-}

{-
 Folding abstract recursion
-}

-- Tail recursion is captured by the foldl function
-- foldl :: Foldable t => (b -> a -> b) -> b -> t a -> b
-- foldl :: (b -> a -> b) -> b -> [a] -> b
-- foldl _ z [] = z
-- foldl f z (x : xs) = foldl f (f z x) xs

foldlSum :: [Integer] -> Integer
foldlSum = foldl (+) 0

{-
 foldlSum [2,3, 5, 7]
 foldl (+) 0 [2, 3, 5, 7]
 foldl (+) (0 + 2) [3, 5, 7]
 foldl (+) ((0 + 2) + 3) [5, 7]
 foldl (+) (((0 + 2) + 3) + 5) [7]
 foldl (+) ((((0 + 2) + 3) + 5) + 7) []
 (((0 + 2) + 3) + 5) + 7
 ((2 + 3) + 5) + 7
 (5 + 5) + 7
 10 + 7
 17
-}

-- Data.Foldable.foldl' :: (b -> a -> b) -> b -> t a -> b
-- has a strict application of the operator: more efficent.
{-
 foldl' (+) 0 [2, 3, 5, 7]
 foldl' (+) 2 [3, 5, 7]
 foldl' (+) 5 [5, 7]
 foldl' (+) 10 [7]
 foldl' (+) 17 []
-}

-- foldr is non-tail recursive
-- foldr :: Foldable t => (a -> b -> b) -> b -> t a -> b
-- foldr :: Foldable t => (a -> b -> b) -> b -> [a] -> b
-- foldr f z = go
--   where
--     go [] = z
--     go (x : xs) = f x (go xs)

foldrSum :: [Integer] -> Integer
foldrSum = foldr (+) 0

{-
 foldrSum [2, 3, 5, 7]
 foldr (+) 0 [2, 3, 5, 7]
 2 + foldr (+) 0 [3, 5, 7]
 2 + (3 + (foldr (+) 0 [5, 7]))
 2 + (3 + (5 + (foldr (+) 0 [7])))
 2 + (3 + (5 + (7 + foldr (+) 0 [])))
 2 + (3 + (5 + (7 + 0)))
 2 + (3 + (5 + 7))
 2 + (3 + 12)
 2 + 15
 17
-}
