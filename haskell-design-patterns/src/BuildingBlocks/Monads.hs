{-# LANGUAGE InstanceSigs #-}

module BuildingBlocks.Monads where

-- a simple example of interpreting expressions

data Expr = Lit Int | Div Expr Expr

eval :: Expr -> Int
eval (Lit a) = a
eval (Div a b) = eval a `div` eval b

mnd1, mnd2 :: Int
mnd1 = eval (Lit 42)                 -- 42
mnd2 = eval (Div (Lit 44) (Lit 11))  -- 4

-- Now let's add (naive) capability to deal with errors in our interpreter.

data Try a = Err String | Return a
  deriving Show

-- The refactored evalTry function is now much more syntactically
-- noisy with case statements:

evalTry :: Expr -> Try Int
evalTry (Lit a) = Return a
evalTry (Div a b) =
  case (evalTry a) of
    Err e -> Err e
    Return a' ->
      case (evalTry b) of
        Err e  -> Err e
        Return b' -> divTry a' b'

-- helper function
divTry :: Int -> Int -> Try Int
divTry a b =
  if b == 0
  then Err "Div by Zero"
  else Return (a `div` b)

mnd3, mnd4, mnd5 :: Try Int
mnd3 = evalTry (Lit 42)              
mnd4 = evalTry (Div (Lit 44) (Lit 11))
mnd5 = evalTry (Div (Lit 44) (Lit 0)) 

-- The reason for the noise is that we have to explicitly propagate
-- errors. If (evalTry a) fails, we return Err and bypass evaluation
-- of the second argument.

instance Functor Try where

  fmap :: (a -> b) -> Try a -> Try b
  fmap f (Return x) = Return (f x)
  fmap _ (Err e) = Err e


instance Applicative Try where

  pure :: a -> Try a
  pure x = Return x

  (<*>) :: Try (a -> b) -> Try a -> Try b
  (Return f) <*> (Return x) = Return (f x)
  (Return _) <*> (Err e) = Err e
  (Err e) <*> _ = Err e


instance Monad Try where

  return :: a -> Try a
  return x   = Return x

  fail :: String -> Try a
  fail msg   = Err msg

  (>>=) :: Try a -> (a -> Try b) -> Try b
  Err e    >>= _   = Err e
  Return a >>= f   = f a

evalTry' :: Expr -> Try Int
evalTry' (Lit a) = Return a
evalTry' (Div a b) =
  (evalTry' a) >>= \a' ->
                     (evalTry' b) >>= \b' ->
                                        divTry a' b'

mnd6 :: Try Int
mnd6 = evalTry' (Div (Lit 44) (Lit 0))  

evalTry'' :: Expr -> Try Int
evalTry'' (Lit a) = Return a
evalTry'' (Div a b) = do
  a' <- evalTry'' a
  b' <- evalTry'' b
  divTry a' b'

mnd7 :: Try Int
mnd7 = evalTry'' (Div (Lit 44) (Lit 0))  

-- The Try data type helped us make failure more explicit, while
-- making it an instance of Monad made it easier to work with. In this
-- same way, Monad can be used to make many other "effects" more
-- explicit.


-- Composing monads and structuring programs. As useful as monads are
-- for capturing effects, we also need to compose them, for example,
-- how do we use monads for failure, I/O, and logging together? In the
-- same way that functional composition allows us to write more
-- focused functions that can be combined together, monad composition
-- allows us to create more focused monads, to be recombined in
-- different ways. In Chapter 3: Patterns for Composition, we will
-- explore monad transformers and how to create "monad stacks" of
-- transformers to achieve monad composition.
