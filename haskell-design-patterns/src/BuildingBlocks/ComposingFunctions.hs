module BuildingBlocks.ComposingFunctions where

cf1 :: (c -> d) -> (b -> c) -> (a -> b) -> a -> d
cf1 f g h =
  -- (.) :: (b -> c) -> (a -> b) -> a -> c
  f . g . h 

