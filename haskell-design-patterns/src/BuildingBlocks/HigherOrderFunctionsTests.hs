{-# LANGUAGE TemplateHaskell #-}

module BuildingBlocks.HigherOrderFunctionsTests where

import           Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import BuildingBlocks.HigherOrderFunctions (square)

prop_square :: Property
prop_square =
  property $ do
    xs <- forAll $ Gen.list (Range.linear 0 100) (Gen.integral (Range.constantFrom 0 (-1000) 1000))
    map square xs === map (\x -> x * x) xs

tests :: IO Bool
tests =
  -- checkSequential $$(discover)
  checkParallel $$(discover)

-- tests :: IO Bool
-- tests =
--   checkParallel $ Group "Test.Example" [
--       ("prop_reverse", prop_reverse)
--     ]
