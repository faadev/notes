module BuildingBlocks.HigherOrderFunctions where

-- A function is a value
square :: Integer -> Integer
square = \x -> x * x

-- Functions can be passed as arguments to other functions (callled
-- higher-order functions)
hof1 :: [Integer]
hof1 =
  -- map :: (a -> b) -> [a] -> [b]
  map square [1, 3, 5, 7]

-- Functions can be returned as a result of another function
hof2 :: [Integer] -> Integer
hof2 =
  -- foldr :: Foldable t => (a -> b -> b) -> b -> t a -> b
  foldr (+) 0

-- Functions can be values of other data structures
hof3 :: [Integer]
hof3 =
  -- zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
  -- ($) :: (a -> b) -> a -> b
  zipWith ($) fs [1, 3, 5]
  where
    -- | A list of functions that take an Integer and return an Integer
    fs :: [Integer -> Integer]
    fs = [(* 2), (* 3), (* 5)] 
