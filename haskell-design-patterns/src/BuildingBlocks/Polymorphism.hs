{-# LANGUAGE InstanceSigs #-}
module BuildingBlocks.Polymorphism where

import BuildingBlocks.HigherOrderFunctions (square)

{-
 - Algebraic Data Types: a very concise way to model composite types, even
 recursive ones. An algebra of types with sum and product as operators.
 - Pattern matching.
 - Polymorphism: parametric and ad-hoc.
-}


{-
 Algebraic types and pattern matching
-}

type Name = String
type Age = Int
data Person = P Name Age -- a composite of combination (a product of types)

data MaybeInt = NoInt | JustInt Int -- a composite of alternative (a sum of types)

maybeInts :: [MaybeInt]
maybeInts = [JustInt 2, JustInt 3, JustInt 5, NoInt]

-- Generic types by parameterizing ADT: Int becomes a (ANY type)
data Maybe' a = Nothing' | Just' a

-- Pattern matching on ADT data constructor is deconstruction

fMaybe :: (a -> b) -> Maybe' a -> Maybe' b
fMaybe f (Just' x) = Just' (f x)
fMaybe _ Nothing' = Nothing'

fMaybes :: [Maybe' Integer]
fMaybes = map (fMaybe (* 2)) [Just' 2, Just' 3, Nothing']

-- Recursive Types

{-
 OO Composite Pattern is 'captured' with Recursive ADT. This pattern
 describes the need to sometimes unify a composite structure with
 individual members of that structure. In this case, we're unifying
 Leaf (a leaf being a part of a tree) and Tree (the composite
 structure).
-}

data Tree a = Leaf a | Branch (Tree a) (Tree a)

-- Functions over recursive types are typically recursive themselves.
size :: Num b => Tree a -> b
size (Leaf _) = 1
size (Branch l r) = size l + size r + 1

{-
 Polymorphism: parametric and ad-hoc (first described by Strachey in
 Fundamental Concepts in Programming Languages, 1967)
-}

-- This function is defined on a list af any type. With this level of
-- abstraction the actual type never comes into play, yet the result
-- is of a particular type. The length function exhibits parametric
-- polymorphism because it acts uniformly on a range of types that
-- share a common structure, in this case, lists. In this case length
-- is a generic function.
length' :: [a] -> Int
length' [] = 0
length' (_ : xs) = 1 + length xs

length_of_list_of_ints :: Int
length_of_list_of_ints = length' ([2,3,4,5] :: [Int])

length_of_list_of_char :: Int
length_of_list_of_char = length' ['2','3','4','5']

-- The canonical example of adhoc polymorphism (also known as
-- overloading) is that of the polymorphic + operator, defined for all
-- types that implement the Num typeclass. When we call (+) on two
-- numbers, the compiler will dispatch evaluation to the concrete
-- implementation, based on the types of numbers being added

x_int :: Int
x_int = 1 + 1

x_float :: Float
x_float = 1.0 + (2.5 :: Float)  

x_double :: Double
x_double = 1 + 3.14  

-- Note that for example 1 is a type-class polymorphic value.  1 is a
-- generic value; whether 1 is to be considered an int or a float
-- value (or a fractional, say) depends on the context in which it
-- will appear.

-- Unify Shape with an algenraic sum of types.
data Shape = Circle Float | Rect Float Float

-- area is ad-hoc polymorphic over the values (data constructor) of
-- Shape. Alternation based polymorphism.
area :: Shape -> Float
area (Circle r) = pi * r * r
area (Rect l w) = l * w

abp1 :: Float
abp1 = area (Circle 2)

abp2 :: Float
abp2 = area (Rect 3 4)


-- Class based ad-hoc polymorphism

data Circle' = Circle' Float
data Rect' = Rect' Float Float

-- Unify two types with the class Shape
class Shape' a where
  area' :: a -> Float

instance Shape' Circle' where
  area' :: Circle' -> Float
  area' (Circle' r) = pi * r * r

instance Shape' Rect' where
  area' :: Rect' -> Float
  area' (Rect' l w) = l * w

cbp1, cbp2 :: Float
cbp1 = area' (Circle' 2)
cbp2 = area' (Rect' 3 4)

cbp3 :: Shape' a => a -> Float
cbp3 = area'

cbp4, cbp5 :: Float
cbp4 = cbp3 (Circle' 2)
cbp5 = cbp3 (Rect' 3 4)

{-
  	
- Alternation-based

  - Different coupling between function and type
    - The function type refers to the algebraic type Shape and then defines
    - special cases for each alternative.

  - Distribution of function definition
    - The overloaded functions are defined together in one place for all
    - alternations.

  - Adding new types
    - Adding a new alternative to the algebraic type requires changing all
    - existing functions acting directly on the algebraic "super type"

  - Adding new functions
    - A perimeter function acting on Shape won't be explicitly related to
    - area in any way.

  - Type expressivity
    - This approach is useful for expressing simple type hierarchies.

- Class-based

  - Different coupling between function and type
    - The function type is only aware of the type it is acting on, not the
    - Shape "super type".

  - Distribution of function definition
    - Overloaded functions all appear in their respective class implementations.
    - This means a function can be overloaded in very diverse parts of the
    - codebase if need be.

  - Adding new types
    - We can add a new type that implements the type class without changing
    - any code in place (only adding). This is very important since it
    - enables us to extend third-party code.

  - Adding new functions
    - A perimeter function could be explicitly related to area by adding it to
    - the Shape class. This is a powerful way of grouping functions together.

  - Type expressivity
    - We can have multiple, orthogonal hierarchies, each implementing the type
    - class (For example, we can express multiple-inheritance type relations).
    - This allows for modeling much richer data types.
-}

-- Polymorphic dispatch and the visitor pattern

-- While exploring adhoc polymorphism, we found 2 ways to express
-- static type dispatch in Haskell (where static means that the
-- dispatch is resolved at compile time, as opposed to dynamic
-- dispatch, which is resolved only at runtime).

-- This is similar to type dispatching: if type is Int do this, if
-- type is Bool do that

-- So far, we have only seen dispatching on one argument or single
-- dispatch. Let's explore what double-dispatch might look like:

data CustomerEvent = InvoicePaid Float | InvoiceNonPayment
data Customer = Individual Int | Organisation Int

-- The payment_handler function defines behavior for all four
-- permutations of CustomerEvent and Customer. In an OOP language, we
-- would have to resort to the visitor pattern to achieve multiple
-- dispatch.
payment_handler :: CustomerEvent -> Customer -> String
payment_handler (InvoicePaid amt) (Individual _) = "SendReceipt for " ++ (show amt)
payment_handler (InvoicePaid amt) (Organisation _) = "SendReceipt for " ++ (show amt)
payment_handler InvoiceNonPayment (Individual custId) = "CancelService for " ++ (show custId)
payment_handler InvoiceNonPayment (Organisation custId) = "SendWarning for " ++ (show custId)

{-
 Unifying parametric and ad-hoc polymorphism
-}

-- In parametric polymorphism a single generic function acts on a
-- variety of types. In adhoc polymorphism an overloaded function is
-- resolved to a particular function implementation by the
-- compiler. Parametric polymorphism allows us to lift the level of
-- abstraction, whereas ad-hoc polymorphism gives us a powerful tool
-- for decoupling. In class Eq this distinction is blurred.


-- Strategy Pattern: lets the algorithm vary independently from
-- clients that use it. In OOD the strategy pattern uses delegation to
-- vary an algorithm.

-- Here, we are defining an abstract algorithm by letting the caller
-- pass in functions as arguments, functions that complete the detail
-- of our algorithm. This corresponds to the strategy pattern, also
-- concerned with decoupling an algorithm from the parts that may
-- change.
strategy :: Monad m => m a -> m b -> m ()
strategy setup1 teardown1 = do
  _ <- setup1
  -- fullfil this function's purpose
  _ <- teardown1
  return ()

-- Template Pattern: Define the skeleton of an algorithm in an
-- operation, deferring some steps to subclasses. Template Method lets
-- subclasses redefine certain steps of an algorithm without changing
-- the algorithm's structure. In OOD the template pattern uses
-- inheritance to vary parts of an algorithm. We might easily abstract
-- an algorithm with the following typeclass that acts as an abstract
-- class

class TemplateAlgorithm a where
  setup2 :: a -> IO a
  teardown2 :: a -> IO a
  doWork2 :: a -> a
  fulfillPurpose :: a -> IO a
  fulfillPurpose x = do
    y <- setup2 x
    let z = doWork2 y
    teardown2 z

-- Iterator Pattern: Provide a way to access the elements of an
-- aggregate object sequentially without exposing its underlying
-- representation.

-- We have decoupled flow control from function application, which is
-- akin to the iterator pattern. The map function takes care of
-- navigating the structure of the list, while the square function
-- only deals with each element of the list.
it1 :: [Integer]
it1 = map square [2, 3, 5, 7] 

-- Decoupling behavior and modularizing code.Whenever we pass one
-- function into another, we are decoupling two parts of code. Besides
-- allowing us to vary the different parts at different rates, we can
-- also put the different parts in different modules, libraries, or
-- whatever we like.
