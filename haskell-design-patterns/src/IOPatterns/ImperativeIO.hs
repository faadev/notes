module IOPatterns.ImperativeIO where

import System.IO (openFile, IOMode(ReadMode), hClose
                 , hGetLine
                 , hIsEOF
                 --, Handle
                 )
import qualified Data.ByteString as B (hGet, unpack)
import qualified Data.ByteString.Char8 as B8
import Data.Char (chr)

main :: IO ()
main = do
  h <- openFile "jabberwocky.txt" ReadMode
  loop h
  hClose h
  where
    loop h' = do   
      -- hIsEOF :: Handle -> IO Bool
      isEof <- hIsEOF h'
      if isEof
        then putStrLn "DONE..."
        else do
        line  <- hGetLine h'
        print $ words line
        loop h'

main1 :: IO ()
main1 = do
  h <- openFile "jabberwocky.txt" ReadMode
  loop h
  hClose h
  where
    loop h' = do   
      isEof <- hIsEOF h'
      if isEof
        then putStrLn "DONE..."
        else do
        -- B.hGet :: Handle -> Int -> IO B.ByteString
        -- | read from the file in chunks of 8 bytes
        chunk' <- B.hGet h' 8
        print . words $ show chunk'
        loop h'

-- | The splitting of a chunk into words is not meaningful anymore. We
-- need to accumulate the chunks until we reach the end of a line and
-- then capture the accumulated line and possible remainder
data Chunk =
  Chunk {chunk :: String}
  | LineEnd {chunk :: String, remainder :: String}
  deriving (Show)
-- chunk :: Chunk -> String
-- remainder :: Chunk -> String

-- | If a chunk contains a newline character, then we return a
-- LineEnd; otherwise, we return just another Chunk:
parseChunk :: B8.ByteString -> Chunk
parseChunk chunk'' =
  if rightS == B8.pack ""
  then Chunk (toS leftS)
  else LineEnd (toS leftS) ((toS . B8.tail) rightS)
  where
    (leftS, rightS) = B8.break (== '\n') chunk''
    toS = map (chr . fromEnum) . B.unpack

main2 :: IO ()
main2 = do
  print $ (parseChunk (B8.pack "AAA\nBB"))
  -- LineEnd {chunk = "AAA", remainder = "BB"}
  print $ (parseChunk (B8.pack "CCC"))
  -- Chunk {chunk = "CCC"}

main3 :: IO ()
main3 = do
  fileH <- openFile "jabberwocky.txt" ReadMode
  loop "" fileH
  hClose fileH
    where
      loop acc h = do
        isEof <- hIsEOF h
        if isEof
          then do putStrLn acc; putStrLn "DONE..."
          else do
          chunk_ <- B.hGet h 8
          case (parseChunk chunk_) of
            (Chunk chunk') -> do
              let accLine = acc ++ chunk' 
              loop accLine h
            (LineEnd chunk' remainder_) -> do 
              let line = acc ++ chunk'
              putStrLn line -- do something with line
              loop remainder_ h
              return ()

{-
 Handle-based I/O has some beneficial characteristics:

 Processing is incremental (for example, the processing of a file).
    We have precise control over resources (for example, when files
    are opened or closed and when long-running processes are started).

The downsides of handle-based I/O are:

    I/O is expressed at a relatively low level of abstraction.  This
    style of code is not very composable; for example, in the previous
    code, we interleave the iteration of the file with the processing
    of the chunks.  The traversal state is exposed: we need to pass
    the file handle around and check for EOF at each iteration. We
    need to explicitly clean up the resource.
-}
