module IOPatterns.FirstClassCitizen where

import System.IO (hClose, hGetLine, IOMode(ReadMode), openFile
                 , Handle
                 )
import Control.Monad (liftM)
--import Control.Applicative

{-
 The IO typeclass (instance of Monad) provides the context in which
 side effects may occur, and it also allows us to decouple pure code
 from I/O code. In this way, side effects are isolated and made
 explicit.
-}

main :: IO ()
main = do
  -- openFile :: FilePath -> IOMode -> IO Handle
  h <- openFile "jabberwocky.txt" ReadMode
  -- hGetLine :: Handle -> IO String
  -- words :: String -> [String]
  -- show :: Show a => a -> String
  -- putStrLn :: String -> IO ()
  hGetLine h >>= putStrLn . show . words 
  -- hClose :: Handle -> IO ()
  hClose h

-- IO is a first-class citizen: functions can return IO actions;
-- functions can take IO actions as arguments. We can compose regular
-- functions with functions that return IO actions.

{-
 I/O as a functor, applicative, and monad
-}

ioAsFunctor, ioAsFunctor' :: Handle -> IO String
ioAsFunctor h = fmap (show . words) (hGetLine h)
ioAsFunctor' h = (show . words) <$> (hGetLine h)

ioAsApplicative :: Handle -> IO String
ioAsApplicative h = pure (show . words) <*> (hGetLine h)

ioasMonad :: Handle -> IO String
ioasMonad h =
  -- liftM :: Monad m => (a -> b) -> m a -> m b
  liftM (show . words) (hGetLine h)

-- However, as we will see, Monad is more powerful than Applicative,
-- and Applicative is more powerful than Functor.
