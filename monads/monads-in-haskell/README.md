From [Monads in Haskell](https://www21.in.tum.de/teaching/perlen/WS1415/unterlagen/Monads_in_Haskell.pdf).

A *functor* is a **structure preserving mapping** from one
**category** to another (like *homomorphisms* in graph theory).

If **Hask** is the category of types and functions, we consider only
functors from Hask to Hask. Such a functor is a pair of functions `f0
:: Types -> Types` and `f1 :: Functions -> Functions` where Types are
all the types in Hask and Functions are all the functions of type `a
-> b` in Hask. A functor must satisfy the following:

1. If `g :: a -> b` then `f1 g :: f0 a -> f0 b`
2. preserve **identity**: forall a . `f1 ida = idf0a`
3. preserve **composition**: If `g :: b -> c` and `h :: a -> b` then
   `f1 (g . h) = f1 g . f1 h`

For example the *usual* `List :: a -> List a` and `map :: (a -> b) ->
(List a -> List b)` form a functor.

`f0` is not a regular function but a **type constructor**.

If `f` is a Functor, then `f1` *lifts* a function of type `a -> b` to
a function of type `f a -> f b`.

![A functor in Hask](a-functor-in-hask.png "A functor in Hask")

A *monad* lifts a function of type `a -> m b` to a function of type `m
a -> m b` where m is a type constructor.

![Monadic lifting](monadic-lifting.png "Monadic lifting")

A monad is a triple `m :: Types -> Types`, `return :: a -> m a` and
`bind :: (a -> m b) -> (m a -> m b)` satisfying:

1. `bind return = id`
2. `bind f . return = f`
3. `bind g . bind f = bind (bind g . f)`

![Second monad law](second-monad-law.png "Second monad law")

![Third monad law](third-monad-law.png "Third monad law")

What the three monad laws say is that the resulting functions should
behave in a way that is somehow similar to what the original functions
did.

In Haskell monads are defined through the *typeclass* `Monad` with a
method called `(>>=) :: Monad m => m a -> (a -> m b) -> m b` and that
can be defined in term of `bind` as `x >>= f = bind f x`.

``` haskell
class Applicative m => Monad (m :: * -> *) where
  (>>=) :: m a -> (a -> m b) -> m b
  (>>) :: m a -> m b -> m b
  return :: a -> m a
  fail :: String -> m a
  {-# MINIMAL (>>=) #-}

class Functor f => Applicative (f :: * -> *) where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b
  liftA2 :: (a -> b -> c) -> f a -> f b -> f c
  (*>) :: f a -> f b -> f b
  (<*) :: f a -> f b -> f a
  {-# MINIMAL pure, ((<*>) | liftA2) #-}

class Functor (f :: * -> *) where
  fmap :: (a -> b) -> f a -> f b
  (<$) :: a -> f b -> f a
  {-# MINIMAL fmap #-}
```

The `>>` operator can be defined as `x >> y = x >>= (\_ -> y)`. It is
used when we want to *sequence* two moandic *actions* whilst
discarding the value of the first one.

The function `join :: Monad m => m (m a) -> m a` can be defined as
`join x = x >>= id` removes one layer of monadic structure. It is
possible to give an alternative definitions of a monad in terms of
only `return`, `join` and `fmap`. 

`m >>= k = join (fmap k m)`

The three laws become:

1. `x >>= return = x`
2. `return a >>= f = f a`
3. `(x >>= f) >>= g = x >>= (\a -> (f a) >>= g)`

A Monad type is always also an Applicative and Functor type:

* `fmap f x = x >>= return . f`
* `pure = return`
* `(<*>) = ap = \m1 m2 -> m1 >>= (\f -> m2 >>= (\x -> return f x))`

The type `IO a` can be viewed as a *computation*, that when run, produces
a value of the type `a`, and possibly has side-effects on the outside world.
Using `>>=` (or the equivalent do notation), makes it possible to
*sequence* these computations in a way that allows the order in which
they are performed to be specified. 
