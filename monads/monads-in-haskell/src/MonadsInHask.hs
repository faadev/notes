module MonadsInHask where

-- `return :: Monad m => a -> m a` maps an element of a type to a
-- corresponding element of the monadic type.

-- `(=<<) :: Monad m => (a -> m b) -> m a -> m b`

bind :: Monad m => (a -> m b) -> m a -> m b
bind = (=<<)

-- Monad laws

-- 1. `bind (return :: a -> m a) = id :: (m a -> m a)`. Implies that
-- `(bind (return :: b -> m b)) . (bind (f :: a -> m b)) = bind f`. It
-- is like `bind return` is a right identity for function composition
-- `(.)`. `bind` lifts `return` to `id` in a monadic world (`m a -> m
-- a`). Applying `bind` to `return :: a -> m a` should return `id :: m
-- a -> m a`.

rightIdentity :: Monad m => m a -> m a
rightIdentity = bind return

-- 2. `bind f . (return :: a -> m a) = f :: (a -> m b)`. Applying
-- `retun :: a -> m a` before `bind f` should return `f`. 

leftIdentity :: Monad m => (a -> m b) -> (a -> m b)
leftIdentity k = bind k . return

-- 3. `bind (g :: b -> m c) . bind (f :: a -> m b) = bind ((bind g)
-- . f)`. Lifing the composition of an unlifted function f with a
-- lifted function g is the same as combining the lifted version of
-- both functions.

-- g :: Monad m => b -> m c
-- f :: Monad m => a -> m b

composition1 :: Monad m => (b -> m c) -> (a -> m b) -> m a -> m c
composition1 g f = bind g . bind f
-- bind g :: Monad m => m b -> m c
-- bind f :: Monad m => m a -> m b

composition2 :: Monad m => (b -> m c) -> (a -> m b) -> m a -> m c
composition2 g f = bind (bind g . f)
-- bind g . f :: Monad m => a -> m c

someFunc :: IO ()
someFunc = do
  let a = 42 :: Int
      ma = [42] :: [Int]
      kab :: Num a => a -> [a]
      kab x = [x + 1]
      kbc :: Num a => a -> [a]
      kbc x = [x + 2]
  print $ rightIdentity ma == ma
  print $ leftIdentity kab a == kab a
  print $ composition1 kbc kab ma == composition2 kbc kab ma


