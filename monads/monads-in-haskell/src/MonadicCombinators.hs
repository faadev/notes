module MonadicCombinators where

import Control.Monad (liftM2)

-- The functions of the form liftM, liftM2, liftM3,. . . are used to
-- “Promote a function to a monad, scanning the monadic arguments from
-- left to right”.

-- liftM :: Monad m => (a -> b) -> m a -> m b
-- liftM = fmap

-- liftM2 :: Monad m => (a -> b -> c) -> m a -> m b -> m c
-- liftM2 f x y = f <$> x <*> y

cartesian_product :: Monad m => m a -> m b -> m (a, b)
cartesian_product = liftM2 (,)

-- The sequence function turns a Traversable t of monadic values into a monadic
-- value over t.

-- sequence :: (Traversable t, Monad m) => t (m a) -> m (t a)

foo :: Maybe [Integer]
foo = sequence [Just 1, Just 2, Just 3]
-- returns Just [1,2,3], but
foo2 :: Maybe [Integer]
foo2 = sequence [Just 1, Just 2, Just 3, Nothing]
-- gives Nothing. 

sequence' :: Applicative f => [f a] -> f [a]
sequence' [] = pure []
sequence' (x:xs) = (:) <$> x <*> sequence' xs

-- creates an IO action that reads in 5 lines from standard input, and
-- gives back a list of String (wrapped in IO) containing these lines.
foo3 :: IO [String]
foo3 = sequence $ replicate 5 getLine
  
  
