{-# LANGUAGE InstanceSigs #-}
module Examples where

import Prelude hiding (Maybe(..))

-- Identity (as in Data.Functor.Identity base)

newtype Identity a = Identity a
  deriving (Eq)

instance Monad Identity where
  (>>=) :: Identity a -> (a -> Identity b) -> Identity b
  (Identity a) >>= k = k a
  
instance Applicative Identity where
  pure :: a -> Identity a
  pure a = Identity a
  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  (Identity f) <*> (Identity a) = Identity (f a)
  
instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap f (Identity a) = Identity (f a)

identityMonadLaws :: IO ()
identityMonadLaws = do
  let a = 42 :: Int
      x = Identity a :: Identity Int
      f :: Show a => a -> Identity String
      f = Identity . show
      g :: String -> Identity Int
      g = Identity . length
  print $ (x >>= return) == x
  print $ (return a >>= f) == f a
  print $ ((x >>= f) >>= g) == (x >>= (\i -> (f i) >>= g))

-- Maybe (as defined in Prelude (Data.Maybe) base)

data Maybe a = Nothing | Just a
  deriving (Eq, Show)

instance Monad Maybe where
  (>>=) :: Maybe a -> (a -> Maybe b) -> Maybe b
  Nothing >>= _ = Nothing
  Just a >>= k = k a
  
instance Applicative Maybe where
  pure :: a -> Maybe a
  pure = Just
  (<*>) :: Maybe (a -> b) -> Maybe a -> Maybe b
  Nothing <*> _ = Nothing
  _ <*> Nothing = Nothing
  Just f <*> Just a = Just (f a)
  
instance Functor Maybe where
  fmap :: (a -> b) -> Maybe a -> Maybe b
  fmap _ Nothing = Nothing
  fmap f (Just a) = Just (f a)

-- The Maybe monad is used for combining functions that can return
-- some kind of “failure” result (called Nothing), with the intent
-- that once a failure has occurred in a chain of such functions, the
-- whole result should be Nothing.

maybeMonadLaws :: IO ()
maybeMonadLaws = do
  let a = 42 :: Int
      x = Just a :: Maybe Int
      f :: Int -> Maybe String
      f = Just . show
      g :: String -> Maybe Int
      g = Just . length
  print $ (Nothing >>= return) == (Nothing :: Maybe Int)
  print $ (x >>= return) == x
  print $ (return a >>= f) == f a
  print $ ((Nothing >>= f) >>= g) == (Nothing >>= (\i -> (f i) >>= g))
  print $ ((x >>= f) >>= g) == (x >>= (\i -> (f i) >>= g))

div12by :: Integral a => a -> Maybe a
div12by x = if x == 0 then Nothing else Just (div 12 x)  

foo :: Maybe Integer
foo = return 2 >>= div12by >>= div12by >>= div12by

foo2 :: Maybe Integer
foo2 = return 13 >>= div12by >>= div12by >>= div12by

-- List (like [] in Data.List base)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

instance Monad List where
  (>>=) :: List a -> (a -> List b) -> List b
  Nil >>= _ = Nil
  as >>= k = concatL (mapL k as)
  
instance Applicative List where
  pure :: a -> List a
  pure a = Cons a Nil
  (<*>) :: List (a -> b) -> List a -> List b
  Nil <*> _ = Nil
  _ <*> Nil = Nil
  fs <*> as = concatL (mapL (\f -> mapL f as) fs)
  
instance Functor List where
  fmap :: (a -> b) -> List a -> List b
  fmap = mapL

mapL :: (a -> b) -> List a -> List b
mapL _ Nil = Nil
mapL f (Cons a as) = Cons (f a) (mapL f as)

appendL :: List a -> List a -> List a
appendL Nil xs = xs
appendL (Cons a as) xs = Cons a (appendL as xs)

concatL :: List (List a) -> List a
concatL Nil = Nil
concatL (Cons a as) = appendL a (concatL as)

-- The List monad is used to model composition of non-deterministic
-- functions. In this context, the term “non-determinism” is used to
-- refer to functions that return a list of results, which can be
-- interpreted as the set of possible return values for a
-- function]. Composing two non-deterministic functions should
-- therefore result in a function that returns a list consisting of
-- the concatenation of all lists returned by the second function
-- when applied to each of the results of the first function.


-- Function (like (->) or Reader monad)

data Function r a = Function (r -> a)

instance Monad (Function r) where
  (>>=) :: Function r a -> (a -> Function r b) -> Function r b
  (Function g) >>= k = Function (\r ->
                                   let Function rb = k (g r)
                                   in rb r)
                       
instance Applicative (Function r) where
  pure :: a -> Function r a
  pure a = Function (\_ -> a)
  (<*>) :: Function r (a -> b) -> Function r a -> Function r b
  Function rab <*> Function ra = Function (\r -> rab r (ra r))
                                             
instance Functor (Function r) where
  fmap :: (a -> b) -> Function r a -> Function r b
  fmap f (Function g) = Function (f . g)

-- This Monad is also refered to as the reader monad, as the functions
-- that are combined with this monad “read from a common source”  

foo3 :: Function Double Double
foo3 = Function (+ 5) >>= (\f1 ->
                             Function (/ 2) >>= (\f2 ->
                                                   return (f1 * f2))) 

foo4 :: Double -> Double
foo4 x =
  let (Function f) = foo3
  in f x

-- The reader monad makes it possible to combine functions that are
-- missing one parameter in a way that the resulting function passes
-- its parameter as the parameter to each of the functions. This can
-- be interpreted as combining functions “with context”, where the
-- context is the missing parameter.

-- Do notation

cartesian_product' :: Monad m => m a -> m b -> m (a, b)
cartesian_product' xs ys = xs >>= (\x ->
                                     ys >>= (\y ->
                                               return (x,y)))

cartesian_product :: Monad m => m a -> m b -> m (a, b)
cartesian_product xs ys = do
  x <- xs
  y <- ys
  return (x, y)

cartesian_product2 :: Applicative f => f a -> f b -> f (a, b)
cartesian_product2 xs ys = (,) <$> xs <*> ys

foo5 :: [(Char, Bool)]
foo5 = cartesian_product "abc" [False, True]
  ++ cartesian_product2 "abc" [False, True]    

foo6 :: Maybe (Char, Bool)
foo6 = cartesian_product (Just 'a') (Just False)

-- This highlights the fact that “do notation” is not a way to write
-- an imperative program in Haskell, but rather a way to write
-- programs that make use of monads.
empty_list :: [a] -> [b] -> [(a, b)]
empty_list xs ys = do
  x <- xs
  y <- ys
  _ <- []
  return (x,y)
-- this function *returns* [] and not the evaluation of `return (x,
-- y)` because `[] >>= _ = []`.
empty_list' :: [a] -> [b] -> [(a, b)]
empty_list' xs ys =
  xs >>= (\x ->
             ys >>= (\y ->
                        [] >>= (\_ ->
                                   return (x,y))))

safediv :: Integral a => a -> a -> Maybe a
safediv x y =
  if y == 0 then Nothing else Just (div x y)

safedividetwice :: Integral b => b -> b -> b -> Maybe b
safedividetwice x y z = do
  u <- safediv x y
  safediv u z
